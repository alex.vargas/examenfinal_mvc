﻿using AwSales.Web.XmlHelpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Factoring.Api.Controllers
{
    public class LocacionController : ApiController
    {
        private UbigeoHelper _ubigeo;
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            _ubigeo = new UbigeoHelper(
                Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data"), "Ubigeo.xml")
                );
        }

        [HttpGet]
        public IHttpActionResult Departamentos()
        {
            return Ok(_ubigeo.GetDepartamentos().ToList());
        }

        [HttpGet]
        public IHttpActionResult Provincias(string departamento)
        {
            return Ok(_ubigeo.GetProvincias(departamento).ToList());
        }

        [HttpGet]
        public IHttpActionResult Distritos(string departamento, string provincia)
        {
            return Ok(_ubigeo.GetDistritos(departamento, provincia).ToList());
        }
    }
}
