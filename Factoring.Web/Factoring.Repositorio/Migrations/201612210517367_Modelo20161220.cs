namespace Factoring.Repositorio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modelo20161220 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Empresa",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ruc = c.String(nullable: false, maxLength: 11, unicode: false),
                        RazonSocial = c.String(nullable: false, maxLength: 100, unicode: false),
                        Direccion = c.String(maxLength: 200, unicode: false),
                        Departamento = c.String(maxLength: 60, unicode: false),
                        Provincia = c.String(maxLength: 60, unicode: false),
                        Distrito = c.String(maxLength: 60, unicode: false),
                        Rubro = c.String(maxLength: 100, unicode: false),
                        Usuario = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EmpresaFactura",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmpresaId = c.Int(nullable: false),
                        FacturaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Factura",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Numero = c.String(nullable: false, maxLength: 20, unicode: false),
                        FechaEmision = c.DateTime(),
                        FechaVencimiento = c.DateTime(),
                        FechaCobro = c.DateTime(),
                        RucCliente = c.String(maxLength: 11, unicode: false),
                        RazonSocialCliente = c.String(maxLength: 100, unicode: false),
                        Total = c.Decimal(precision: 15, scale: 3),
                        Impuestos = c.Decimal(precision: 15, scale: 3),
                        RutaFacturaScaneada = c.String(maxLength: 256, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Factura");
            DropTable("dbo.EmpresaFactura");
            DropTable("dbo.Empresa");
        }
    }
}
