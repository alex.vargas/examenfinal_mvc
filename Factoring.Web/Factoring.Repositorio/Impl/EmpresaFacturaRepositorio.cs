﻿using Factoring.Modelo;
using System;
using System.Data.Entity;

namespace Factoring.Repositorio.Impl
{
    public class EmpresaFacturaRepositorio : IDisposable
    {
        private readonly DbContext _bd;

        private readonly RepositorioGenerico<Empresa> _empresas;
        private readonly RepositorioGenerico<Factura> _facturas;
        private readonly RepositorioGenerico<EmpresaFactura> _empresaFacturas;

        public EmpresaFacturaRepositorio(DbContext bd)
        {
            _bd = bd;
            _empresas = new RepositorioGenerico<Empresa>(bd);
            _facturas = new RepositorioGenerico<Factura>(bd);
            _empresaFacturas = new RepositorioGenerico<EmpresaFactura>(bd);
        }

        public RepositorioGenerico<Empresa> Empresas { get { return _empresas; }  }
        public RepositorioGenerico<Factura> Facturas { get { return _facturas; } }
        public RepositorioGenerico<EmpresaFactura> EmpresaFacturas { get { return _empresaFacturas; } }

        public void Commit()
        {
            _bd.SaveChanges();
        }
        public void Dispose()
        {
            _bd.Dispose();
        }
    }
}
