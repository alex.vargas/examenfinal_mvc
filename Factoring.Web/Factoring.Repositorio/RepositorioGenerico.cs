﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Factoring.Repositorio
{
    public class RepositorioGenerico<TEntidad> where TEntidad : class
    {
        private readonly DbContext bd;
        public RepositorioGenerico(DbContext baseDatos)
        {
            this.bd = baseDatos;
        }
        public TEntidad TraerUno(Expression<Func<TEntidad, bool>> predicado)
        {
            return bd.Set<TEntidad>().SingleOrDefault(predicado);
        }

        public IQueryable<TEntidad> TraeVarios(Expression<Func<TEntidad, bool>> predicado)
        {
            return bd.Set<TEntidad>().Where(predicado);

        }

        public IQueryable<TEntidad> TraerTodos()
        {
            return bd.Set<TEntidad>();
        }

        public void Agregar(TEntidad entidad)
        {
            bd.Set<TEntidad>().Add(entidad);
        }

        public void Eliminar(TEntidad entidad)
        {
            bd.Set<TEntidad>().Remove(entidad);
        }

        public void Actualizar(TEntidad entidad)
        {
            if (bd.Entry<TEntidad>(entidad).State == System.Data.Entity.EntityState.Detached)
                bd.Set<TEntidad>().Attach(entidad);

            bd.Entry<TEntidad>(entidad).State = System.Data.Entity.EntityState.Modified;
        }

    }
}
