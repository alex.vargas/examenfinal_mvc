namespace Factoring.Repositorio
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Modelo;

    public partial class EmpresaFacturaContext : DbContext
    {
        public EmpresaFacturaContext()
            : base("name=EmpresaContext")
        {
        }

        public virtual DbSet<Empresa> Empresa { get; set; }
        public virtual DbSet<EmpresaFactura> EmpresaFactura { get; set; }
        public virtual DbSet<Factura> Factura { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Empresa>()
                .Property(e => e.Ruc)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.RazonSocial)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Departamento)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Provincia)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Distrito)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Rubro)
                .IsUnicode(false);

            modelBuilder.Entity<Factura>()
                .Property(e => e.Numero)
                .IsUnicode(false);

            modelBuilder.Entity<Factura>()
                .Property(e => e.RucCliente)
                .IsUnicode(false);

            modelBuilder.Entity<Factura>()
                .Property(e => e.RazonSocialCliente)
                .IsUnicode(false);

            modelBuilder.Entity<Factura>()
                .Property(e => e.Total)
                .HasPrecision(15, 3);

            modelBuilder.Entity<Factura>()
                .Property(e => e.Impuestos)
                .HasPrecision(15, 3);

            modelBuilder.Entity<Factura>()
                .Property(e => e.RutaFacturaScaneada)
                .IsUnicode(false);
        }
    }
}
