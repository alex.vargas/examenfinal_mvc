﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Factoring.Web.Startup))]
namespace Factoring.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
