﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Factoring.Web.Funcionalidad.Factura
{
    public class FacturaViewModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Numero { get; set; }

        public DateTime? FechaEmision { get; set; }

        public DateTime? FechaVencimiento { get; set; }

        public DateTime? FechaCobro { get; set; }

        [StringLength(11)]
        public string RucCliente { get; set; }

        [StringLength(100)]
        public string RazonSocialCliente { get; set; }

        public decimal? Total { get; set; }

        public decimal? Impuestos { get; set; }

        [StringLength(256)]
        public string RutaFacturaScaneada { get; set; }
        public int EmpresaId { get; set; }
    }
}