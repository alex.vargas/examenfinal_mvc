﻿using Factoring.Web.Funcionalidad.Empresa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Factoring.Web.Funcionalidad.Factura
{
    public class EliminaFacturaHandler : BaseEmpresaHandler, IEliminaHandler<FacturaViewModel>
    {
        public EliminaFacturaHandler() : base()
        { }
        public void Elimina(int id)
        {
            var factura = repositorio.Facturas.TraerUno(x => x.Id == id);
            var empfactura = repositorio.EmpresaFacturas.TraerTodos().Where(x => x.FacturaId == id).SingleOrDefault();

            repositorio.Facturas.Eliminar(factura);
            repositorio.EmpresaFacturas.Eliminar(empfactura);

            repositorio.Commit();
        }

        public FacturaViewModel TraeUno(int id)
        {
            return repositorio.Facturas.TraerTodos().Where(x => x.Id == id).Select(f => new FacturaViewModel()
            {
                Id = f.Id,
                Numero = f.Numero,
                FechaEmision = f.FechaEmision,
                FechaCobro = f.FechaCobro,
                FechaVencimiento = f.FechaVencimiento,
                RucCliente = f.RucCliente,
                RazonSocialCliente = f.RazonSocialCliente,
                Total = f.Total,
                Impuestos = f.Impuestos,
                RutaFacturaScaneada = f.RutaFacturaScaneada
            }).SingleOrDefault();
        }
    }
}