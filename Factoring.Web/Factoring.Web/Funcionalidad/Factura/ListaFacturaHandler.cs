﻿using Factoring.Web.Funcionalidad.Empresa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Factoring.Web.Funcionalidad.Factura
{
    public class ListaFacturaHandler : BaseEmpresaHandler, IListaHandler<FacturaViewModel>
    {
        public ListaFacturaHandler() : base()
        { }
        public IEnumerable<FacturaViewModel> Listar(string filtro)
        {
            int empresaId = int.Parse(filtro);

            var query = from factura in repositorio.Facturas.TraerTodos()
                       join empFact in repositorio.EmpresaFacturas.TraerTodos()
                            on factura.Id equals empFact.FacturaId into factGrupo
                       from target in factGrupo
                       where target.EmpresaId == empresaId
                        select new FacturaViewModel()
                       {
                           EmpresaId = target.EmpresaId,
                           Id = target.FacturaId,
                           Numero = factura.Numero,
                           FechaEmision = factura.FechaEmision,
                           FechaCobro = factura.FechaCobro,
                           FechaVencimiento = factura.FechaVencimiento,
                           RucCliente = factura.RucCliente,
                           RazonSocialCliente = factura.RazonSocialCliente,
                           Total = factura.Total,
                           Impuestos = factura.Impuestos,
                           RutaFacturaScaneada = factura.RutaFacturaScaneada
                       };

            return query.ToList();
        }

        public FacturaViewModel TraeUno(int id)
        {
            return repositorio.Facturas.TraerTodos().Where(x => x.Id == id).Select(f => new FacturaViewModel()
            {
                Id = f.Id,
                Numero = f.Numero,
                FechaEmision = f.FechaEmision,
                FechaCobro = f.FechaCobro,
                FechaVencimiento = f.FechaVencimiento,
                RucCliente = f.RucCliente,
                RazonSocialCliente = f.RazonSocialCliente,
                Total = f.Total,
                Impuestos = f.Impuestos,
                RutaFacturaScaneada = f.RutaFacturaScaneada
            }).SingleOrDefault();
        }
    }
}