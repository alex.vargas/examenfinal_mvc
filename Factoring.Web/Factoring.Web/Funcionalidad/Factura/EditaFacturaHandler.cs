﻿using Factoring.Web.Funcionalidad.Empresa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Factoring.Web.Funcionalidad.Factura
{
    public class EditaFacturaHandler : BaseEmpresaHandler, IEditaHandler<RegistraFacturaViewModel>
    {
        public EditaFacturaHandler() : base()
        { }
        public void Actualizar(RegistraFacturaViewModel modelo)
        {
            var factura = repositorio.Facturas.TraerUno(x => x.Id == modelo.Id);

            factura.Numero = modelo.Numero;
            factura.FechaEmision = modelo.FechaEmision;
            factura.FechaCobro = modelo.FechaCobro;
            factura.FechaVencimiento = modelo.FechaVencimiento;
            factura.RucCliente = modelo.RucCliente;
            factura.RazonSocialCliente = modelo.RazonSocialCliente;
            factura.Total = modelo.Total;
            factura.Impuestos = modelo.Impuestos;
            factura.RutaFacturaScaneada = modelo.RutaFacturaScaneada;

            repositorio.Commit();
            //var query = from factura in repositorio.Facturas.TraerTodos()
            //            join empfact in repositorio.EmpresaFacturas.TraerTodos() on factura.Id equals empfact.FacturaId into factGrupo
            //            from target in factGrupo
            //            where factura.Numero == modelo.Numero && target.EmpresaId == modelo.EmpresaId
            //            select target;

            //if (query.Count() > 0)
            //{
            //    throw new Exception("Ya existe esta factura para esta empresa");
            //}
            //else
            //{
            //    var factura = repositorio.Facturas.TraerUno(x => x.Id == modelo.Id);

            //    factura.Numero = modelo.Numero;
            //    factura.FechaEmision = modelo.FechaEmision;
            //    factura.FechaCobro = modelo.FechaCobro;
            //    factura.FechaVencimiento = modelo.FechaVencimiento;
            //    factura.RucCliente = modelo.RucCliente;
            //    factura.RazonSocialCliente = modelo.RazonSocialCliente;
            //    factura.Total = modelo.Total;
            //    factura.Impuestos = modelo.Impuestos;
            //    factura.RutaFacturaScaneada = modelo.RutaFacturaScaneada;

            //    repositorio.Commit();
            //}
        }

        public RegistraFacturaViewModel TraeUno(int id)
        {
            var resultado = from factura in repositorio.Facturas.TraerTodos()
                   join empFact in repositorio.EmpresaFacturas.TraerTodos()
                        on factura.Id equals empFact.FacturaId into factGrupo
                   from target in factGrupo
                   where target.FacturaId == id
                   select new RegistraFacturaViewModel()
                   {
                       EmpresaId = target.EmpresaId,
                       Id = target.FacturaId,
                       Numero = factura.Numero,
                       FechaEmision = factura.FechaEmision,
                       FechaCobro = factura.FechaCobro,
                       FechaVencimiento = factura.FechaVencimiento,
                       RucCliente = factura.RucCliente,
                       RazonSocialCliente = factura.RazonSocialCliente,
                       Total = factura.Total,
                       Impuestos = factura.Impuestos,
                       RutaFacturaScaneada = factura.RutaFacturaScaneada
                   };

            return resultado.SingleOrDefault();
        }
    }
}