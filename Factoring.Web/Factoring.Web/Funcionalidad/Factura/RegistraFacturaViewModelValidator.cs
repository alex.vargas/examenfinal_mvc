﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Factoring.Web.Funcionalidad.Factura
{
    public class RegistraFacturaViewModelValidator : AbstractValidator<RegistraFacturaViewModel>
    {
        public RegistraFacturaViewModelValidator()
        {
            RuleFor(modelo => modelo.Numero)
                .NotEmpty()
                .Length(2, 15)
                .WithMessage("Debe ingresar un numero de factura");
            RuleFor(modelo => modelo.FechaEmision)
                .NotNull()
                .Must(ValidaFechaEmision)
                .WithMessage("Debe Ingresar una fecha de emision menor a la fecha de vencimiento");
            RuleFor(modelo => modelo.FechaVencimiento)
                .NotNull()
                .Must(ValidaFechaVencimiento)
                .WithMessage("Debe ingresar una fecha de vencimiento menor a la fecha de cobro");
            RuleFor(modelo => modelo.FechaCobro)
                .NotNull()
                .WithMessage("Debe ingresar una fecha de cobro");
            RuleFor(modelo => modelo.RucCliente)
                .NotEmpty()
                .Must(ValidaRuc)
                .WithMessage("Debe ingresar un ruc valido");
            RuleFor(modelo => modelo.Total)
                .NotNull()
                .GreaterThan(0)
                .WithMessage("Debe ingresar un monto mayor a cero");
            RuleFor(modelo => modelo.Impuestos)
                .NotNull()
                .GreaterThan(0)
                .Must(ValidaIgv)
                .WithMessage("Debe ingresar un impuesto correcto");
        }

        private bool ValidaFechaEmision(RegistraFacturaViewModel modelo, DateTime? fechaEmision)
        {
            return fechaEmision.HasValue ? fechaEmision.Value.Subtract(modelo.FechaVencimiento.Value).Days <= 0 : false;
        }

        private bool ValidaFechaVencimiento(RegistraFacturaViewModel modelo, DateTime? fechaVencimiento)
        {
            return fechaVencimiento.HasValue ? fechaVencimiento.Value.Subtract(modelo.FechaCobro.Value).Days <= 0 : false;
        }

        private bool ValidaRuc(RegistraFacturaViewModel modelo, string ruc)
        {
            Regex validaDigitoNumerico = new Regex(@"2\d{10}");
            if (!validaDigitoNumerico.IsMatch(ruc)) return false;

            if (ruc.Length != 11) return false;

            var factor = "5432765432";
            int sumaDigitos = 0;
            for (var i = 0; i < factor.Length; i++)
            {
                sumaDigitos += int.Parse(factor.Substring(i, 1)) * int.Parse(ruc.Substring(i, 1));
            }

            int residuo = sumaDigitos % 11;
            int resta = 11 - residuo;

            int digCheck;
            if (resta == 10)
                digCheck = 0;
            else if (resta == 11)
                digCheck = 1;
            else
                digCheck = resta;

            if (digCheck != int.Parse(ruc.Substring(10, 1)))
                return false;

            return true;
        }

        private bool ValidaIgv(RegistraFacturaViewModel modelo, decimal? igv)
        {
            return igv.HasValue ? modelo.Total.Value * 18/100 == igv.Value : false;
        }
    }
}