﻿using Factoring.Web.Funcionalidad.Empresa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Factoring.Web.Funcionalidad.Factura
{
    public class RegistraFacturaHandler : BaseEmpresaHandler, IRegistraHandler<RegistraFacturaViewModel>
    {
        public RegistraFacturaHandler() : base()
        { }
        public void Registrar(RegistraFacturaViewModel modelo)
        {
            var query = from factura in repositorio.Facturas.TraerTodos()
                        join empfact in repositorio.EmpresaFacturas.TraerTodos() on factura.Id equals empfact.FacturaId into factGrupo
                        from target in factGrupo
                        where factura.Numero == modelo.Numero && target.EmpresaId == modelo.EmpresaId
                        select target;

            if (query.Count() > 0)
            {
                throw new Exception("Ya existe esta factura para esta empresa");
            }
            else
            {
                repositorio.Facturas.Agregar(new Modelo.Factura()
                {
                    Numero = modelo.Numero,
                    FechaEmision = modelo.FechaEmision,
                    FechaCobro = modelo.FechaCobro,
                    FechaVencimiento = modelo.FechaVencimiento,
                    RucCliente = modelo.RucCliente,
                    RazonSocialCliente = modelo.RazonSocialCliente,
                    Total = modelo.Total,
                    Impuestos = modelo.Impuestos,
                    RutaFacturaScaneada = modelo.RutaFacturaScaneada
                });

                repositorio.Commit();
                int facturaId = repositorio.Facturas.TraerUno(x => x.Numero == modelo.Numero).Id;

                repositorio.EmpresaFacturas.Agregar(new Modelo.EmpresaFactura()
                {
                    EmpresaId = modelo.EmpresaId,
                    FacturaId = facturaId
                });

                repositorio.Commit();
            }
        }
    }
}