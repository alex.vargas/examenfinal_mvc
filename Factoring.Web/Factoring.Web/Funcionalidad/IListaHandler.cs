﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factoring.Web.Funcionalidad
{
    interface IListaHandler<T> where T : class
    {
        IEnumerable<T> Listar(string filtro);
        T TraeUno(int id);
    }
}
