﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factoring.Web.Funcionalidad
{
    interface IEditaHandler<T> where T : class
    {
        T TraeUno(int id);
        void Actualizar(T modelo);
    }
}
