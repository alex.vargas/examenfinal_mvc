﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factoring.Web.Funcionalidad
{
    interface IRegistraHandler<T> where T:class
    {
        void Registrar(T modelo);
    }
}
