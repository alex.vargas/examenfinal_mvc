﻿using System.ComponentModel.DataAnnotations;

namespace Factoring.Web.Funcionalidad.Empresa
{
    public class EmpresaViewModel
    {

        public int Id { get; set; }

        [Required]
        [StringLength(11)]
        public string Ruc { get; set; }

        [Required]
        [StringLength(100)]
        public string RazonSocial { get; set; }

        [StringLength(200)]
        public string Direccion { get; set; }

        [StringLength(60)]
        public string Departamento { get; set; }

        [StringLength(60)]
        public string Provincia { get; set; }

        [StringLength(60)]
        public string Distrito { get; set; }

        [StringLength(100)]
        public string Rubro { get; set; }

        [Required]
        [StringLength(256)]
        public string Usuario { get; set; }
    }
}