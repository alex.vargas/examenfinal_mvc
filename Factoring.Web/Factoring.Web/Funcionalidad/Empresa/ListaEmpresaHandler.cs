﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Factoring.Web.Funcionalidad.Empresa
{
    public class ListaEmpresaHandler : BaseEmpresaHandler, IListaHandler<EmpresaViewModel>
    {
        public ListaEmpresaHandler() : base()
        { }
        public IEnumerable<EmpresaViewModel> Listar(string filtro)
        {
            return repositorio.Empresas.TraerTodos().Where(x => x.Usuario == filtro).Select(e => new EmpresaViewModel()
            {
                Id = e.Id,
                Ruc = e.Ruc,
                RazonSocial = e.RazonSocial,
                Direccion = e.Direccion,
                Departamento = e.Departamento,
                Provincia = e.Provincia,
                Distrito = e.Distrito,
                Rubro = e.Rubro,
                Usuario = e.Usuario
            }).ToList();
        }

        public EmpresaViewModel TraeUno(int id)
        {
            return repositorio.Empresas.TraerTodos().Where(x => x.Id == id).Select(e => new EmpresaViewModel()
            {
                Id = e.Id,
                Ruc = e.Ruc,
                RazonSocial = e.RazonSocial,
                Direccion = e.Direccion,
                Departamento = e.Departamento,
                Provincia = e.Provincia,
                Distrito = e.Distrito,
                Rubro = e.Rubro,
                Usuario = e.Usuario
            }).SingleOrDefault();
        }
    }
}