﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Factoring.Web.Funcionalidad.Empresa
{
    public class EliminaEmpresaHandler : BaseEmpresaHandler, IEliminaHandler<EmpresaViewModel>
    {
        public EliminaEmpresaHandler() : base()
        { }
        public void Elimina(int id)
        {
            var empresa = repositorio.Empresas.TraerTodos().Where(x => x.Id == id).FirstOrDefault();
            var facturasEmpresa = repositorio.EmpresaFacturas.TraerTodos().Where(x => x.EmpresaId == empresa.Id);
            if (facturasEmpresa.Count() > 0)
            {
                throw new Exception("La empresa tiene facturas registradas");
            }
            else
            {
                repositorio.Empresas.Eliminar(empresa);
                repositorio.Commit();
            }

        }

        public EmpresaViewModel TraeUno(int id)
        {
            return repositorio.Empresas.TraerTodos().Where(x => x.Id == id).Select(e => new EmpresaViewModel()
            {
                Id = e.Id,
                Ruc = e.Ruc,
                RazonSocial = e.RazonSocial,
                Direccion = e.Direccion,
                Departamento = e.Departamento,
                Provincia = e.Provincia,
                Distrito = e.Distrito,
                Rubro = e.Rubro,
                Usuario = e.Usuario
            }).SingleOrDefault();
        }
    }
}