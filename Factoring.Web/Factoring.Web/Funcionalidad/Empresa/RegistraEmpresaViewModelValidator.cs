﻿using FluentValidation;
using System;
using System.Text.RegularExpressions;
using System.Linq;

namespace Factoring.Web.Funcionalidad.Empresa
{
    public class RegistraEmpresaViewModelValidator : AbstractValidator<RegistraEmpresaViewModel>
    {
        public RegistraEmpresaViewModelValidator()
        {
            RuleFor(modelo => modelo.RazonSocial)
                .NotEmpty()
                .Length(2, 100)
                .WithMessage("La razon social no puede estar vacia");

            RuleFor(modelo => modelo.Direccion)
                .NotEmpty()
                .Length(2, 200)
                .WithMessage("La direccion no puede estar vacia");

            RuleFor(modelo => modelo.Ruc)
                //.NotEmpty()
                .Must(ValidaRuc)
                .WithMessage("El ruc no es valido");


        }

        private bool ValidaRuc(RegistraEmpresaViewModel modelo, string ruc)
        {
            Regex validaDigitoNumerico = new Regex(@"2\d{10}");
            if (!validaDigitoNumerico.IsMatch(ruc)) return false;

            if (ruc.Length != 11) return false;

            var factor = "5432765432";
            int sumaDigitos = 0;
            for (var i = 0; i < factor.Length; i++)
            {
                sumaDigitos += int.Parse(factor.Substring(i, 1)) * int.Parse(ruc.Substring(i, 1));
            }

            int residuo = sumaDigitos % 11;
            int resta = 11 - residuo;

            int digCheck;
            if (resta == 10)
                digCheck = 0;
            else if (resta == 11)
                digCheck = 1;
            else
                digCheck = resta;

            if (digCheck != int.Parse(ruc.Substring(10, 1)))
                return false;

            return true;
        }
    }
}