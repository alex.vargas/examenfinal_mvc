﻿using Factoring.Repositorio;
using Factoring.Repositorio.Impl;
using System;

namespace Factoring.Web.Funcionalidad.Empresa
{
    public class BaseEmpresaHandler : IDisposable
    {
        protected readonly EmpresaFacturaRepositorio repositorio;

        public BaseEmpresaHandler()
        {
            repositorio = new EmpresaFacturaRepositorio(new EmpresaFacturaContext());
        }
        public void Dispose()
        {
            repositorio.Dispose();
        }
    }
}