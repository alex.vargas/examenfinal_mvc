﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Factoring.Web.Funcionalidad.Empresa
{
    public class EditaEmpresaHandler : BaseEmpresaHandler, IEditaHandler<RegistraEmpresaViewModel>
    {
        public EditaEmpresaHandler() :base()
        { }
        public void Actualizar(RegistraEmpresaViewModel modelo)
        {
            var empresa = repositorio.Empresas.TraerUno(x => x.Id == modelo.Id);
            empresa.Ruc = modelo.Ruc;
            empresa.Direccion = modelo.Direccion;
            empresa.Departamento = modelo.Departamento;
            empresa.Provincia = modelo.Provincia;
            empresa.Distrito = modelo.Distrito;
            empresa.Rubro = modelo.Rubro;

            repositorio.Commit();
            //if (repositorio.Empresas.TraerTodos().Where(x => x.Ruc == modelo.Ruc).Count() == 0)
            //{

            //}
            //else
            //{
            //    throw new Exception("Esta empresa ya se encuentra registrada");
            //}
        }

        public RegistraEmpresaViewModel TraeUno(int id)
        {
            return repositorio.Empresas.TraerTodos().Where(x => x.Id == id).Select(e => new RegistraEmpresaViewModel()
            {
                Id = e.Id,
                Ruc = e.Ruc,
                RazonSocial = e.RazonSocial,
                Direccion = e.Direccion,
                Departamento = e.Departamento,
                Provincia = e.Provincia,
                Distrito = e.Distrito,
                Rubro = e.Rubro,
                Usuario = e.Usuario
            }).SingleOrDefault();
        }
    }
}