﻿using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Factoring.Web.Funcionalidad.Empresa
{
    [Validator(typeof(RegistraEmpresaViewModelValidator))]
    public class RegistraEmpresaViewModel
    {
        public int Id { get; set; }

        public string Ruc { get; set; }

        
        
        public string RazonSocial { get; set; }

        
        public string Direccion { get; set; }

        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string Rubro { get; set; }
        
        public string Usuario { get; set; }


        public SelectList Departamentos { get; set; }
        public SelectList Provincias { get; set; }
        public SelectList Distritos { get; set; }

        public RegistraEmpresaViewModel()
        {
            var listaVacia = new List<string>() { "Seleccione..." };
            Departamentos = new SelectList(listaVacia);
            Provincias = new SelectList(listaVacia);
            Distritos = new SelectList(listaVacia);
        }
    }
}