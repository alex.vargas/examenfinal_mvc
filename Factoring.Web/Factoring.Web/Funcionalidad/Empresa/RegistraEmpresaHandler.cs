﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Factoring.Web.Funcionalidad.Empresa
{
    public class RegistraEmpresaHandler : BaseEmpresaHandler, IRegistraHandler<RegistraEmpresaViewModel>
    {
        public RegistraEmpresaHandler() : base()
        { }
        public void Registrar(RegistraEmpresaViewModel modelo)
        {
            if (repositorio.Empresas.TraerTodos().Where(x => x.Ruc == modelo.Ruc).Count() == 0)
            {
                repositorio.Empresas.Agregar(new Modelo.Empresa()
                {
                    Id = modelo.Id,
                    Ruc = modelo.Ruc,
                    RazonSocial = modelo.RazonSocial,
                    Direccion = modelo.Direccion,
                    Departamento = modelo.Departamento,
                    Provincia = modelo.Provincia,
                    Distrito = modelo.Distrito,
                    Rubro = modelo.Rubro,
                    Usuario = modelo.Usuario
                });

                repositorio.Commit();
            }
            else
            {
                throw new Exception("Esta empresa ya se encuentra registrada");
            }
            
        }
    }
}