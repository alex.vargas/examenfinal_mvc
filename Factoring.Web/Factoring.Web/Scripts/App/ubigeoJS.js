﻿var moduloempresa = (function () {
    var urlDpto = 'https://localhost:44359/api/locacion/departamentos';
    var urlProv = 'https://localhost:44359/api/locacion/provincias';
    var urlDist = 'https://localhost:44359/api/locacion/distritos';

    function configuraCombosEnCascadaUbigeo($seccion) {
        if ($seccion.length <= 0) return; // si la seccion no existe no esta vacia
        // configurar
        $seccion.cascadingDropdown({
            selectBoxes: [
                {
                    selector: 'select.departamento',
                    source: function (request, response) {
                        $.ajax(                            {
                                dataType: "json",
                                url: urlDpto,
                                success: function (dptosLista) {
                                    var dpto = $seccion.find('input[type=hidden].departamento').val();

                                    var dptos = $.map(dptosLista, function (item, index) {
                                        return {
                                            label: item,  //item.descricion,
                                            value: item,  //item.codigo,
                                            selected: item === dpto // item.codigo === dpto
                                        }
                                    });
                                    response(dptos);
                                }
                            });
                    }
                },
                {
                    selector: 'select.provincia',
                    requires: ['select.departamento'],
                    source: function (request, response) {
                        $.ajax({
                            dataType: "json",
                            url: urlProv,
                            data: request,
                            success: function (provLista) {
                                var prov = $seccion.find('input[type=hidden].provincia').val();
                                var provs = $.map(provLista, function (item, index) {
                                    return {
                                        label: item,  //item.descricion,
                                        value: item,  //item.codigo,
                                        selected: item === prov // item.codigo === dpto
                                    }
                                });
                                response(provs);
                            }
                        });
                    }
                },
                {
                    selector: 'select.distrito',
                    requires: ['select.departamento', 'select.provincia'],
                    source: function (request, response) {
                        $.ajax({
                            dataType: "json",
                            url: urlDist,
                            data: request,
                            success: function (distLista) {
                                var dist = $seccion.find('input[type=hidden].distrito').val(); // para preseleccionar el dpto
                                var dists = $.map(distLista, function (item, index) {
                                    return {
                                        label: item,  //item.descricion,
                                        value: item,  //item.codigo,
                                        selected: item === dist // item.codigo === dpto
                                    }
                                });
                                response(dists);
                            }
                        });
                    },
                    onChange: function (event, value, requiredValues) {
                        if (value) {
                            $seccion.find('input[type=hidden].departamento').val(requiredValues['departamento']);
                            $seccion.find('input[type=hidden].provincia').val(requiredValues['provincia']);
                            $seccion.find('input[type=hidden].distrito').val(value);

                        }
                    }
                }
            ]
        });
    }

    // publicamos los metodos del modulo

    return {
        vincularControles: function () {
            configuraCombosEnCascadaUbigeo($("#empresa-ubigeo"));
        }
    }
})();

// configuramos la iife

$(function () {
    moduloempresa.vincularControles();
});