﻿using Factoring.Web.Funcionalidad.Empresa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Factoring.Web.Controllers
{
    public class EmpresaController : Controller
    {
        // GET: Empresa
        public ActionResult Lista(string usuario)
        {
            using (var empresa = new ListaEmpresaHandler())
            {
                var user = User.Identity.Name;
                ViewBag.Usuario = user;
                return View(empresa.Listar(user));
            }                
        }

        public ActionResult Ver(int id)
        {
            using (var empresa = new ListaEmpresaHandler())
            {
                return View(empresa.TraeUno(id));
            }
        }

        [HttpGet]
        public ActionResult Agrega(string usuario)
        {
            ViewBag.Usuario = usuario;
            return View(new RegistraEmpresaViewModel() { Usuario = usuario });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Agrega(RegistraEmpresaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);

            using (var empresa = new RegistraEmpresaHandler())
            {
                try
                {
                    empresa.Registrar(modelo);
                    return RedirectToAction("Lista", new { Usuario = modelo.Usuario });
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }
            }
        }

        [HttpGet]
        public ActionResult Edita(int id)
        {
            using (var empresa = new EditaEmpresaHandler())
            {
                return View(empresa.TraeUno(id));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edita(RegistraEmpresaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);

            using (var empresa = new EditaEmpresaHandler())
            {
                try
                {
                    empresa.Actualizar(modelo);
                    return RedirectToAction("Lista", new { Usuario = modelo.Usuario });
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }
            }
        }

        [HttpGet]
        public ActionResult Elimina(int id)
        {
            using (var empresa = new EliminaEmpresaHandler())
            {
                return View(empresa.TraeUno(id));
            }
        }

        [HttpPost]
        public ActionResult EliminaEmpresa(EmpresaViewModel modelo)
        {
            using (var empressa = new EliminaEmpresaHandler())
            {
                try
                {
                    string usuario = modelo.Usuario;
                    empressa.Elimina(modelo.Id);
                    return RedirectToAction("Lista", new { Usuario = usuario });
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }
            }
        }
    }
}