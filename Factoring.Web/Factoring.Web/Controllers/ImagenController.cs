﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Factoring.Web.Controllers
{
    public class ImagenController : Controller
    {
        // GET: Imagen
        [HttpGet]
        public FileResult Muestra(string nombre)
        {
            nombre = nombre.Trim().Length == 0 ? "20161222.png" : nombre;
            var path = Path.Combine(Server.MapPath("~/Upload"), nombre);
            string[] partFiles = nombre.Split(new char[] { '.' });

            return new FileStreamResult(
                new FileStream(path, FileMode.Open),
                MimeMapping.GetMimeMapping($"{partFiles[0]}.{partFiles[1]}"));
        }
    }
}