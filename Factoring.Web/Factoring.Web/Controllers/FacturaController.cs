﻿using Factoring.Web.Funcionalidad.Factura;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Factoring.Web.Controllers
{
    public class FacturaController : Controller
    {
        // GET: Factura
        public ActionResult Lista(string empresaId)
        {
            using (var factura = new ListaFacturaHandler())
            {
                ViewBag.EmpresaId = empresaId;
                return View(factura.Listar(empresaId));
            }
            
        }

        public ActionResult Ver(int id, int empresaId)
        {
            using (var factura = new ListaFacturaHandler())
            {
                var obFact = factura.TraeUno(id);
                obFact.EmpresaId = empresaId;
                return View(obFact);
            }
        }

        [HttpGet]
        public ActionResult Agrega(string empresaId)
        {
            return View(new RegistraFacturaViewModel() { EmpresaId = int.Parse(empresaId) });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Agrega(RegistraFacturaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);

            using (var factura = new RegistraFacturaHandler())
            {
                try
                {
                    string path = string.Empty;

                    if (modelo.Archivos.Count > 0)
                    {
                        var archivo = modelo.Archivos[0];
                        
                        var fileName = Path.GetFileName(archivo.FileName);
                        var fileExt = Path.GetExtension(fileName);
                        var id = Guid.NewGuid().ToString("N");

                        path = Path.Combine(Server.MapPath("~/Upload"), string.Join("", id, fileExt));
                        archivo.SaveAs(path);
                        modelo.RutaFacturaScaneada = string.Join("", id, fileExt);
                    }

                    factura.Registrar(modelo);
                    return RedirectToAction("Lista", new { EmpresaId = modelo.EmpresaId });
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }
            }
        }

        [HttpGet]
        public ActionResult Edita(int id, int empresaId)
        {
            ViewBag.EmpresaId = empresaId;
            using (var factura = new EditaFacturaHandler())
            {
                var obFactura = factura.TraeUno(id);
                obFactura.EmpresaId = empresaId;
                return View(obFactura);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edita(RegistraFacturaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);

            using (var factura = new EditaFacturaHandler())
            {
                try
                {
                    string path = string.Empty;

                    if (modelo.Archivos.Count > 0)
                    {
                        var archivo = modelo.Archivos[0];
                        
                        
                        var fileName = Path.GetFileName(archivo.FileName);
                        var fileExt = Path.GetExtension(fileName);
                        var id = Guid.NewGuid().ToString("N");

                        path = Path.Combine(Server.MapPath("~/Upload"), string.Join("", id, fileExt));
                        archivo.SaveAs(path);
                        modelo.RutaFacturaScaneada = string.Join("", id, fileExt);
                    }
                    
                    factura.Actualizar(modelo);
                    return RedirectToAction("Lista", new { EmpresaId = modelo.EmpresaId });
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }
            }
        }

        [HttpGet]
        public ActionResult Elimina(int id, int empresaId)
        {
            using (var factura = new EliminaFacturaHandler())
            {
                var obFactura = factura.TraeUno(id);
                obFactura.EmpresaId = empresaId;
                return View(obFactura);
            }
        }

        [HttpPost]
        public ActionResult Elimina(FacturaViewModel modelo)
        {
            using (var factura = new EliminaFacturaHandler())
            {
                try
                {
                    int empresaId = modelo.EmpresaId;
                    factura.Elimina(modelo.Id);
                    return RedirectToAction("Lista", new { EmpresaId = empresaId.ToString() });
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }
            }
        }
    }
}