namespace Factoring.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("EmpresaFactura")]
    public partial class EmpresaFactura
    {
        public int Id { get; set; }

        public int EmpresaId { get; set; }

        public int FacturaId { get; set; }
    }
}
